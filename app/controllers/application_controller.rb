class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
      render html: "hello, world!"
  end

  def hallo
      render html: "Hallo Welt!"
  end
end
